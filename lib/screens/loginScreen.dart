import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text("Khorocher Hisab"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 150),
              child: Text(
                "Sign In",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 44.0),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            GestureDetector(
              onTap: () {
                return print("card tapped");
              },
              child: Card(
                color: Colors.blue.shade900,
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  leading: Image.asset(
                    "images/facebook.png",
                    scale: 0.5,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(left: 50.0),
                    child: Text(
                      "Sing In with Facebook",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                return print("card tapped");
              },
              child: Card(
                color: Colors.blue.shade900,
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  leading: Image.asset(
                    "images/facebook.png",
                    scale: 0.5,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(left: 50.0),
                    child: Text(
                      "Sing In with Facebook",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                return print("card tapped");
              },
              child: Card(
                color: Colors.blue.shade900,
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  leading: Image.asset(
                    "images/facebook.png",
                    scale: 0.5,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(left: 50.0),
                    child: Text(
                      "Sing In with Facebook",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.symmetric(horizontal: 5.0),
              child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.yellowAccent,
                  ),
                  onPressed: () {},
                  child: Text(
                    "Go anonymous",
                    style: TextStyle(fontSize: 20.0),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
